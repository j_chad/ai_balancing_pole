# AI Balancing Pole

Reinforcement Learning: Testing policies using a vertical pole balanced on a moving platform as an analogy; gym environment - https://gym.openai.com/

10 policies tested to see which can balance the pole most successfully

## Version
* 1.0.0
* September 2018

## Dependencies

### Python
* Python 3.6.5
* NumPy 1.15.1
* Gym 0.10.5
* Flask 1.0.2

#### Author
* James Chadwick, 2018
* Derived from an article by Mike Shi on Medium
